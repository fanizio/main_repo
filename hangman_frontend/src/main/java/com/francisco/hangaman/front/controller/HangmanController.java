package com.francisco.hangaman.front.controller;

import com.francisco.hangaman.front.service.HangmanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@Component
@ManagedBean
@SessionScoped
public class HangmanController
{

    @Autowired
    private HangmanService hangmanService;

    private int mistakes = 0;

    private String challegeWord;
    private List<String> letters = new ArrayList<String>();

    public void onInitializeGame()
    {
        challegeWord = hangmanService.getChallengeWord();
        letters.clear();
        mistakes = 0;
    }

    public boolean isGameInitialized()
    {
        return challegeWord != null;
    }

    public boolean isGameCompleted()
    {
        if (challegeWord == null || letters.isEmpty())
        {
            return false;
        }

        for (int i = 0; i < challegeWord.length(); i++)
        {
            if (!letters.contains(challegeWord.substring(i, i + 1)))
            {
                return false;
            }
        }

        return true;
    }

    public String getMaskedWord()
    {
        if (challegeWord == null)
        {
            return "";
        }

        if (letters.isEmpty())
        {
            return generateMask(challegeWord);
        }

        // get the positions of letters we already know
        final List<Integer> positions = new ArrayList<>();

        for (int i = 0; i < challegeWord.length(); i++)
        {
            for (final String letter : letters)
            {
                if (challegeWord.charAt(i) == letter.toUpperCase().charAt(0))
                {
                    positions.add(i);
                }
            }
        }

        // generate mask with new known letter
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < challegeWord.length(); i++)
        {
            boolean hasUpdateChar = false;

            for (final Integer position : positions)
            {
                if (i == position)
                {
                    sb.append(challegeWord.charAt(i)).append(" ");
                    hasUpdateChar = true;
                }
            }

            if (!hasUpdateChar)
            {
                sb.append("_ ");
            }
        }
        return sb.toString().trim();
    }

    public String getLastEnteredLetter()
    {
        return "";
    }

    public void setLastEnteredLetter(final String lastEnteredLetter)
    {
        if ( lastEnteredLetter != null && lastEnteredLetter.trim().length() > 0 )
        {
            final String letter = lastEnteredLetter.substring( lastEnteredLetter.length() - 1).toUpperCase();

            int pos = challegeWord.indexOf(letter);

            if (pos == -1)
            {
                mistakes ++;
            }

            if ( ! letters.contains(letter) )
            {
                letters.add(letter);
            }
        }
    }

    public int getMistakes()
    {
        return mistakes;
    }

    private static String generateMask(final String word)
    {
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < word.length(); i++)
        {
            sb.append("_ ");
        }
        return sb.toString().trim();
    }
}