package com.francisco.hangaman.front;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import java.util.Arrays;


@SpringBootApplication
public class HangmanApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(HangmanApplication.class, args);
    }

    @Bean
    public ServletRegistrationBean jsfServletRegistration (ServletContext servletContext)
    {
        //spring boot only works if this is set
        servletContext.setInitParameter("com.sun.faces.forceLoadConfiguration", Boolean.TRUE.toString());

        //registration
        ServletRegistrationBean srb = new ServletRegistrationBean();
        srb.setServlet(new FacesServlet());
        srb.setUrlMappings(Arrays.asList("*.xhtml"));
        srb.setLoadOnStartup(1);
        return srb;
    }


    public void addViewControllers(ViewControllerRegistry registry)
    {
        registry.addViewController("/").setViewName("index.xhtml");
    }

}