package com.francisco.hangaman.front.model;

import java.io.Serializable;

public class HangmanChallengeWord implements Serializable
{
    public void setChallengeWord(String challengeWord)
    {
        this.challengeWord = challengeWord;
    }

    public HangmanChallengeWord(String challengeWord)
    {
        this.challengeWord = challengeWord;
    }

    public HangmanChallengeWord()
    {

    }

    public String getChallengeWord() {
        return challengeWord;
    }

    private String challengeWord;


}
