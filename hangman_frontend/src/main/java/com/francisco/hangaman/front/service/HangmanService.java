package com.francisco.hangaman.front.service;


import com.francisco.hangaman.front.model.HangmanChallengeWord;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HangmanService
{

    private final String GET_CHALLENGEWORD_ENDPOINT_URL = "http://localhost:8001/word";

    public String getChallengeWord()
    {

        RestTemplate restTemplate = new RestTemplate();

        HangmanChallengeWord cw  = restTemplate.getForObject(GET_CHALLENGEWORD_ENDPOINT_URL, HangmanChallengeWord.class);


        return cw.getChallengeWord();
    }
}