# Hangman Game

This project is the implementation of hangman game by using Java, Spring and JSF.

To run this application you must have installed java 8 or above and apache maven 3 or above and follow the next steps.

I - The folder hangman-backend is the backend part of whole solution.

To run the backend using the command $ mvn spring-boot:run.

you can check if everything is ok on the url : http://localhost:8001/word


II - The folder hangman-frontend is the frontend part of whole solution.

To run the frontend using the command $ mvn spring-boot:run.

you can check if everything is ok on the url : http://localhost:9001/index.xhtml

## PLAY THE GAME

The game must be played on the application http://localhost:9001/index.xhtml