package com.francisco.hangaman.util.xml;

import com.francisco.hangaman.model.Hangman;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;


public class XMLParser
{
	public static Hangman castXMLtoClass(String xmlData) throws JAXBException
	{
		JAXBContext jaxbContext = JAXBContext.newInstance(Hangman.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (Hangman) jaxbUnmarshaller.unmarshal(new StringReader(xmlData));
	}
}
