package com.francisco.hangaman.util.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReaderUtil
{
	public static String getContent(String url) throws IOException
	{
		return new String(Files.readAllBytes(Paths.get(url)));
	}
}