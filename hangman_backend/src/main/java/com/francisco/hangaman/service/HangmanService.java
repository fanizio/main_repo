package com.francisco.hangaman.service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.bind.JAXBException;

import com.francisco.hangaman.util.xml.XMLParser;
import com.francisco.hangaman.model.Hangman;
import com.francisco.hangaman.rest.ChallengeWord;
import com.francisco.hangaman.util.io.FileReaderUtil;
import org.springframework.stereotype.Service;

@Service
public class HangmanService
{
	private ChallengeWord word = null;

	public ChallengeWord getChallengeWord() throws IOException, JAXBException
	{
		String fileContent = FileReaderUtil.getContent("src/main/resources/available-words.xml");

		if( fileContent != null )
		{
			Hangman hangman = XMLParser.castXMLtoClass(fileContent);
			word = getRandomWordFromList(hangman);
		}
		return word;
	}

	private ChallengeWord getRandomWordFromList(Hangman hangman)
	{
		List<String> words = hangman.getWords();

		int random = ThreadLocalRandom.current().nextInt(0, words.size());

		return new ChallengeWord(words.get(random));
	}
}