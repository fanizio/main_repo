package com.francisco.hangaman.rest;

public class ChallengeWord
{
	private String challengeWord;
	public ChallengeWord(String challengeWord)
	{
		super();
		this.challengeWord = challengeWord;
	}

	public String getChallengeWord()
	{
		return challengeWord;
	}

	public void setChallengeWord(String word)
	{
		this.challengeWord = word;
	}
}