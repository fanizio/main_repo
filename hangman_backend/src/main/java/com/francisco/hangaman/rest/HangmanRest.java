package com.francisco.hangaman.rest;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.francisco.hangaman.service.HangmanService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HangmanRest
{
	@Autowired
	private HangmanService hangmanService;

	@CrossOrigin(origins = "http://localhost:8001")
	@GetMapping("/word")
	public ChallengeWord getChallengeWord() throws IOException, JAXBException
	{
		return hangmanService.getChallengeWord();
	}
}