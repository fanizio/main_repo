package com.francisco.hangman.test;

import java.nio.file.NoSuchFileException;
import java.util.Arrays;

import javax.xml.bind.JAXBException;

import com.francisco.hangaman.model.Hangman;
import com.francisco.hangaman.util.io.FileReaderUtil;
import com.francisco.hangaman.util.xml.XMLParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;


class XMLParserTests {
	
	private String xmlData;
	private Hangman hangmanExample;

	@BeforeEach
	void setUp() throws Exception
	{
		xmlData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><hangman><word_list><word>COMMITMENT</word></word_list></hangman>";
		
		hangmanExample = new Hangman();
		hangmanExample.setWords(Arrays.asList("COMMITMENT"));
	}

	@Test
	void xmlCastSuccessfull()
	{
		Hangman result = null;
		try {
			result = XMLParser.castXMLtoClass(xmlData);
		} catch (JAXBException e) {
			fail(e.getMessage());
		}
		
		assertEquals(hangmanExample, result);
		
	}

	@Test
	public void InvalidXml()
	{
		xmlData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><chess>word_list>??<word>KING</word></word_list></chess>";
		assertThrows(JAXBException.class, () -> { final Hangman result = XMLParser.castXMLtoClass(xmlData); });
	}

}
