package com.francisco.hangman.test;

import com.francisco.hangaman.HangmanApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = HangmanApplication.class )
class HangmanApplicationTests
{
	@Test
	void contextLoads() {
	}

}
