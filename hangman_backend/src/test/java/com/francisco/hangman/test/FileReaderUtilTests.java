package com.francisco.hangman.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

import com.francisco.hangaman.util.io.FileReaderUtil;
import org.junit.jupiter.api.Test;

public class FileReaderUtilTests
{

	String unexistingFile = "not_exists_file.xml";
	String existingFile = "src/main/resources/application.properties";
	
	@Test
	public void fileNotFound()
	{
		Exception exception = assertThrows(NoSuchFileException.class, () -> { FileReaderUtil.getContent(unexistingFile); });
        assertEquals(exception.getMessage(), unexistingFile);
	}
	
	@Test
	public void fileReaderSuccess()
	{
		String result = null;
		
		try
		{
			result = FileReaderUtil.getContent(existingFile);
		}
		catch (IOException e)
		{
			fail(e.getMessage());
		}			

        assertEquals("server.port=8001", result);
	}
}